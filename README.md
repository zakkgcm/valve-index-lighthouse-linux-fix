# Valve Index Linux Lighthouse Fix
Contains a script and optional udev/systemd files to toggle the Valve Index Lighthouse/Base Stations on Linux. Only tested on 2.0 lighthouses.

Needed since native support is [broken](https://github.com/ValveSoftware/SteamVR-for-Linux/issues/207).

## set_lighthouse
Toggles Valve Index Lighthouse Base Stations, with auto retry. Requires `gattool`.

On Arch, `gattool` can found in `bluez-utils-compat` in the AUR.

Before using, set the bluetooth MAC addresses of your lighthouses in the script using the procedure described in the link below.

Based on info from [here](https://biechl.net/post/details/5).

## udev rules + systemd service
Automatically runs set_lighthouse when the Index HMD is plugged in. Uses a udev rule to bind a systemd service to the HMD device.

Install the udev rule to: `/etc/udev/rules.d/` and the systemd service to `/etc/systemd/system/`. The service assumes `set_lighthouse` is installed in /usr/local/bin.

Based on info from [here](https://unix.stackexchange.com/questions/378133/why-is-my-udev-fired-script-not-working) and incorporated a workaround from [here](https://github.com/systemd/systemd/issues/7587#issuecomment-381428545) to get udev and systemd to play nice together.
